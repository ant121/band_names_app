class Band {
  final String? id;
  final String? name;
  final int? votes;

  const Band({this.id, this.name, this.votes});

  factory Band.fromMap(Map<String, dynamic> object) =>
      Band(id: object['id'], name: object['name'], votes: object['votes']);
}
